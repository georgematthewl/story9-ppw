function getData() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;

    $.ajax({
        method: 'GET',
        url: '/render/',
        headers: {
            "X-CSRFToken": csrftoken,
        },
        success: function (result) {
            var data = result.data;
            var icon;
            for (var i = 0; i < data.length; i++) {
                var book_info = data[i];
                if (book_info.boolean) {
                    icon = `<button id="${book_info['id']}" onclick='removeFavorites("${book_info['id']}")'><i class="yellow"></i></button>`;
                } else {
                    icon = `<button id="${book_info['id']}" onclick='addFavorites("${book_info['id']}")'><i class="def"></i></button>`;
                }
                html =
                    `<tr><td align='center'><img width='80' height='100' src='${book_info['url']}'></td><td>${book_info['title']}</td><td>${book_info['authors']}</td><td>${book_info['published']}</td><td align="center">${icon}</td></tr>`;
                $('#tbody').append(html);
            }
        }
    })
}

function addFavorites(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/add/",
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {
            id: id,
        },
        success: function (res) {
            var icon = `<button id="${res.id}" onclick='removeFavorites("${res.id}")'><i class="yellow"></i></button>`;
            $("#" + res.id).replaceWith(icon);
            $("#counter").replaceWith(`<span id='counter'>${res.count}</span>`);
        },
        error: function () {
            alert("Error, cannot get data from server")
        }
    });
}

function removeFavorites(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/remove/",
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {
            id: id,
        },
        success: function (res) {
            var icon = `<button id="${res.id}" onclick='addFavorites("${res.id}")'><i class="def"></i></button>`;
            $('#' + res.id).replaceWith(icon);
            $("#counter").replaceWith(`<span id='counter'>${res.count}</span>`);
        },
        error: function () {
            alert("Error, cannot get data from server")
        }
    });
}

$(document).ready(function () {
    gapi.load('auth2', function () {
        gapi.auth2.init();
    });
});

function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    sendToken(id_token);
    console.log("Logged In");
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

function sendToken(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;

    $.ajax({
        method: "POST",
        url: "/login/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id_token: token},
        success: function (result) {
            console.log("Successfully send token");
            if (result.status === "0") {
                html = "<h4 class='success'>Logged In</h4>";
                window.location.replace(result.url);
            } else {
                html = "<h4 class='fail'>Something error, please report</h4>"
            }
            $("h4").replaceWith(html)
        },
        error: function () {
            alert("Something error, please report")
        }
    })
}

