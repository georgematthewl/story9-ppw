from django.shortcuts import render
from google.oauth2 import id_token
from google.auth.transport import requests
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse


# Create your views here.
def login(request):
    if request.method == 'POST':
        try:
            token = request.POST['id_token']
            id_info = id_token.verify_oauth2_token(token, requests.Request(),
                                                   '123463855450-fbctk2q3d0fnmt219l0o4oclphv2n10f.apps.googleusercontent.com')
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')

            user_id = id_info['sub']
            name = id_info['name']
            email = id_info['email']
            request.session['user_id'] = user_id
            request.session['name'] = name
            request.session['email'] = email
            request.session['books'] = []

            return JsonResponse({'status': '0', 'url': reverse('main')})
        except ValueError:
            return JsonResponse({'status': '1'})
    return render(request, 'login.html')


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login-page'))
