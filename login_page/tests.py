from django.test import TestCase
from django.test import Client


# Create your tests here.

class LoginPageUnitTest(TestCase):

    def test_main_page_not_logged_landing_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_lab11_false_token(self):
        response = Client().post('/login/', {'id_token': "testtttt"}).json()
        self.assertEqual(response['status'], "1")

    def test_lab11_right_token(self):
        response = Client().post('/login/', {
            'id_token': "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ2M2ZlNDgwYzNjNTgzOWJiYjE1ODYxZTA4YzMyZDE4N2ZhZjlhNTYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMTIzNDYzODU1NDUwLWZiY3RrMnEzZDBmbm10MjE5bDBvNG9jbHBodjJuMTBmLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMTIzNDYzODU1NDUwLWZiY3RrMnEzZDBmbm10MjE5bDBvNG9jbHBodjJuMTBmLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTE2NTY5NjYyNzMxMDM2MDgyNTUyIiwiZW1haWwiOiJtYXR0aGV3LmxpbW9uZ2FuQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiTVNSN3g5MVFESkFPbjFBenlFTmU1QSIsIm5hbWUiOiJHZW9yZ2UgTWF0dGhldyIsInBpY3R1cmUiOiJodHRwczovL2xoNS5nb29nbGV1c2VyY29udGVudC5jb20vLTNBNEtCZ0RKc1IwL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FHRGd3LWpkUzB5M2tlQTBCUkxNQ1FueFctTm90b0pobGcvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6Ikdlb3JnZSIsImZhbWlseV9uYW1lIjoiTWF0dGhldyIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNTQzNDY1MjQxLCJleHAiOjE1NDM0Njg4NDEsImp0aSI6IjM3ZDRkNDQ5MTJjNjA4Y2JhZTJiODg3N2MxYjI0ODQ3NDhmMDA5ODUifQ.DAZhvVLgIGlGOu3c7opXDV5dpOG73A1ZYZPo6kOtmDIz7eg1_jH8VV8KNt4O5SffZkjzdd_e1p_r3_2o9DC6MhdeGQZotUaaubFTxRyiKwQOVuP-fkpFbwpjdt99M39nWCO3S5wLksNof8_YElK2TQlMMuD90jDvpUCGRHCEN8nLHSP033CrYqnUdIJsKQlZYBwfZrqCqVbeypgxtX01Iq1wPooD5P7NeyzrNui_O3JHK7GVUT2tVxxh043ASLF9i_vcdC61lzzbHkSq-mvaRjuBSe30GsXBY02IE5ZbodZt5FA2-ddpnysEFYsWwHKGGMWrxr_kontm1As1V5QS8w"}).json()
        self.assertEqual(response['status'], "0")

    def test_logout(self):
        response = Client().get('/login/logout/')
        self.assertEqual(response.status_code, 302)
