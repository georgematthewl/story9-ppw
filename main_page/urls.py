from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name="main"),
    path('render/', get_data, name="render-books-data"),
    path('add/', add_to_favorites, name="add-favorites"),
    path('remove/', remove_favorites, name="remove-favorites"),

    path('ajax/', get_ajax, name="get-ajax"),
    path('challenge/', challenge, name="challenge-page"),
]
