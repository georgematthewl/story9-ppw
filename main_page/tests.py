from importlib import import_module

import requests
from django.conf import settings
from django.test import TestCase, Client
from django.urls import resolve
from django.http import request
from .views import index, handler500


# Create your tests here.
class SessionTestCase(TestCase):
    def setUp(self):
        settings.SESSION_ENGINE = "django.contrib.sessions.backends.file"
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key


class UnitTestStory9(SessionTestCase):
    def test_url_not_login_main_page_redirect_to_other_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)

    def test_url_login_main_page(self):
        test = self.client.session
        test['user_id'] = 'jorj'
        test['name'] = 'jorj'
        test.save()

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_challenge_page_is_exist(self):
        response = Client().get('/challenge/')
        self.assertEqual(response.status_code, 200)

    def test_main_page_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_client_can_post_and_render(self):
        s = self.client.session
        s['user_id'] = 'jorjjjj'
        s['name'] = 'jorj'
        s.save()
        response_post = self.client.post('/add/', {'id': 'abcd'})
        self.assertEqual(response_post.status_code, 200)

    def test_client_can_delete_and_delete_selected(self):
        title = "helpp"
        s = self.client.session
        s['user_id'] = 'jorj'
        s['name'] = 'jorj'
        s['books'] = [title]
        s.save()
        response_post = self.client.post('/remove/', {'id': title})
        self.assertEqual(response_post.status_code, 200)

    def test_client_can_get_data(self):
        s = self.client.session
        s['user_id'] = 'hehe'
        s['name'] = 'lala'
        s['books'] = []
        s.save()
        response = self.client.get('/render/')
        self.assertEqual(response.status_code, 200)

    def test_books_already_favorited(self):
        s = self.client.session
        s['user_id'] = 'hehe'
        s['name'] = 'lala'
        s['book'] = []
        s.save()
        get = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()
        items = get['items']
        book = items[0]
        dataId = book['id']
        title = book['volumeInfo']['title']
        self.client.post('/add/', {'id': dataId})
        response = self.client.get('/render/').json()
        bool = response['data'][0]['boolean']
        self.assertTrue(bool)

    def test_books_not_favorited(self):
        s = self.client.session
        s['user_id'] = 'hehe'
        s['name'] = 'lala'
        s['books'] = []
        s.save()
        response = self.client.get('/render/').json()
        bool = response['data'][0]['boolean']
        self.assertFalse(bool)

    def test_get_ajax(self):
        get = Client().get('/ajax/?query=quilting').json()
        items = get['items'][0]
        title = items['volumeInfo']['title']
        self.assertEqual(items['volumeInfo']['title'], title)

    def test_404_page(self):
        response = Client().get('/aa')
        self.assertEqual(response.status_code, 404)

    def test_500_page(self):
        response = handler500(request)
        self.assertEqual(response.status_code, 200)
